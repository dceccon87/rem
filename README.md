# REM - Rack Environment Monitor

This is a simple project for monitor a rack temperature and humidity.

The goal is to have a simple sistem for have a datalog of rack temperature and start fan, with a frequency modulation, when internal temperature is to much high.

## Components
* [Arduino Uno](https://store.arduino.cc/arduino-uno-rev3);
* [Arduino Ethernet Shield](https://www.arduino.cc/en/Main/ArduinoEthernetShieldV1);
* AM2302/DHT22 (x2);
* Relay 2 channel;
* [Noctua NF-F12](https://noctua.at/en/nf-f12-pwm) (x2).

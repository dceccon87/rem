#define REM_VERSION 1
//#define REM_DEBUG

//DHTx library
#include "DHT.h"
//Ethernet schield required library
#include <SPI.h>
#include <Ethernet.h>

/**
 * Period data structure
 */
struct period_data_t {
  int8_t hum_int; /**<  Humidity internal */
  int8_t hum_ext; /**<  Humidity external */
  int8_t temp_int_i;  /**<  Temperature internal integer  */
  int8_t temp_ext_i;  /**<  Temperature external integer  */
  uint8_t temp_int_f: 4;  /**<  Temperature internal deciaml */
  uint8_t temp_ext_f: 4;  /**<  Temperature external deciaml  */
  uint8_t pwm_1;  /**<  PWM timer 1 */
  uint8_t pwm_2;  /**<  PWM timer 2 */
};

/**
 * PINS
 */
#define DHT_INT_PIN 2 /**<  DHT internal PIN number */
#define DHT_EXT_PIN 3 /**<  DHT external PIN number */
#define RELAY1_PIN 4  /**<  Relay 1 PIN */
#define RELAY2_PIN 5  /**<  Relay 2 PIN */
#define LED_BLUE_PIN  6 /**<  Led blue PIN  */
#define LED_GREEN_PIN  7  /**<  Led green PIN */
#define LED_RED_PIN  8  /**<  Led red PIN */
#define OC1A_PIN  ((byte)9) /**<  Timer 1 PIN */
#define OC1B_PIN  ((byte)10)  /**<  Timer 2 PIN */
/**
 * Temperature setup
 */
#define TEMP_DELTA  ((float)0.5f) /**<  Temperature delta value for set PWM */
#define FAN1_RPM_DELTA ((byte)10)  /**<  PWM percentile step */
/**
 * Period setup
 */
#define READ_INST_S ((unsigned int)10)  /**<  Seconds for read current values */
#define READ_FOR_PERIOD_CNT ((unsigned int)30)  /**<  Number of read to do for period */
#define PERIOD_DATA_CNT (unsigned int)(((8 * 60) / (READ_INST_S * READ_FOR_PERIOD_CNT)) * 60) /**<Number of read for period */
#define FAN_UP_PERIOD_S  ((unsigned int)120) /**<  FAN up period */
/**
 * PWM fans timer
 */
const word PWM_FREQ_kHZ = 25; /**<  Fan frequency in kHz  */
const word TCNT1_TOP = 16000/(2*PWM_FREQ_kHZ);  /**< PWM frequency set for fan  */
/**
 * Loop flags and counters
 */
unsigned int tick_i = 0;  /**<  Main loop tick counter  */
unsigned int tick_read_s = 0; /**<  Number of seconds from last sensors read  */
unsigned int fan_up_s = 0;  /**<  Number of seconds of fan is up  */
/**
 * Fan setup
 */
byte fan_1_rpm = 0; /**<  PWM 1 frequency in percentile */
byte fan_2_rpm = 0; /**<  PWM 2 frequency in percentile */
bool fan_to_start = false;  /**<  Fan flag for start  */
/**
 * Period values
 */
float temp_int_period = 0;  /**<  Internal temperature of current period  */
float hum_int_period = 0; /**<  Internal humidity of current period*/
float temp_ext_period = 0;  /**<  External temperature of current period  */
float hum_ext_period = 0; /**<  External humidity of current period*/
unsigned int read_for_period = 0;
period_data_t period_data[PERIOD_DATA_CNT];
unsigned int period_data_i = 0;
unsigned int period_data_cnt = 0;

/**
 * Ethernet setup
 */
bool eth_enable = false;  /**<  Ethernet shield enable status */
// Ethernet MAC address
byte eth_mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
// Ethernet IP address
IPAddress eth_ip(192, 168, 3, 250);
// Ethernet server library
EthernetServer eth_server(80);
//Init internal DHT device
DHT dht_int(DHT_INT_PIN, DHT22);
//Init external DHT device
DHT dht_ext(DHT_EXT_PIN, DHT22);

/**
 * Setup function
 * 
 * Start and initialize all device and configuration.
 */
void setup() {
  // Initialize Ethernet by CS pin
  Ethernet.init(10);

  // Open serial communications and wait for port to open:
  #ifdef REM_DEBUG
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.println("Debug serial: READY");
  #endif

  // Start Ethernet connection
  Ethernet.begin(eth_mac, eth_ip);
  // Check for Ethernet hardware present
  if (Ethernet.hardwareStatus() != EthernetNoHardware) {
    #ifdef REM_DEBUG
    Serial.println("Ethernet hardware: READY");
    #endif
    // Start Ethernet server
    eth_server.begin();
    #ifdef REM_DEBUG
    Serial.print("Ethernet server: UP at ");
    Serial.println(Ethernet.localIP());
    #endif
    eth_enable = true;
  }
  #ifdef REM_DEBUG
  else {
    Serial.println("Ethernet hardware: NOT READY");
  }
  #endif

  //Setup Timer for PWM
  pinMode(OC1A_PIN, OUTPUT);
  pinMode(OC1B_PIN, OUTPUT);
  // Clear Timer1 control and count registers
  TCCR1A = 0;
  TCCR1B = 0;
  TCNT1  = 0;

  TCCR1A |= (1 << COM1A1) | (1 << WGM11);
  TCCR1B |= (1 << WGM13) | (1 << CS10);
  ICR1 = TCNT1_TOP;

  setPwmDuty(OC1A_PIN, 0);
  setPwmDuty(OC1B_PIN, 0);

  //Start DHT
  dht_int.begin();
  dht_ext.begin();
  //Reset relay pins
  pinMode(RELAY1_PIN, OUTPUT);
  pinMode(RELAY2_PIN, OUTPUT);
  digitalWrite(RELAY1_PIN, HIGH);
  digitalWrite(RELAY2_PIN, HIGH);
  //Reset led pins
  pinMode(LED_RED_PIN, OUTPUT);
  pinMode(LED_GREEN_PIN, OUTPUT);
  pinMode(LED_BLUE_PIN, OUTPUT);
  digitalWrite(LED_RED_PIN, LOW);
  digitalWrite(LED_GREEN_PIN, LOW);
  digitalWrite(LED_BLUE_PIN, LOW);

  //clean data
  memset(period_data, 0, sizeof(period_data_t) * PERIOD_DATA_CNT);
}

/**
 * Main loop function
 */
void loop() {
  //Set a millisecond delay
  delay(1);
  //Loop tick counter update
  ++tick_i;

  // Manage what to do after 1 second
  if (tick_i > 1000) {
    #ifdef REM_DEBUG
    Serial.print("+");
    #endif
    //Reset tick, for net sencond...
    tick_i = 0;
    //Update read period seconds
    ++tick_read_s;
    //If fan is up, update seconds
    if (fan_up_s > 0)
    {
      --fan_up_s;
    }
  }

  // Sensors read after period
  if (tick_read_s >= READ_INST_S) {
    tick_read_s = 0;
    //Read themperatures
    // internal
    float t_i = dht_int.readTemperature() - 1.0f;
    float h_i = dht_int.readHumidity() + 1.0f;
    // external
    float t_e = dht_ext.readTemperature() + 2.0f;
    float h_e = dht_ext.readHumidity() + 4.0f;

    #ifdef REM_DEBUG
    Serial.println();
    Serial.print("T_i: ");
    Serial.print(t_i);
    Serial.print(" T_e: ");
    Serial.print(t_e);
    Serial.print(" | H_i: ");
    Serial.print(h_i);
    Serial.print(" H_e: ");
    Serial.println(h_e);
    #endif

    //Check temperature for mangage fans
    if (fan_up_s < 1 && (t_i - t_e) > 0) {
      //FAN modulation iff temperature...
      if(t_i > 20){
          //Calculate fan new rpm by temperature difference
        if(round(t_i - t_e) <= 10){
          fan_1_rpm = (byte)((round(t_i - t_e) / TEMP_DELTA) * FAN1_RPM_DELTA);
        }
        else{
          fan_1_rpm = 100;
        }
      }
      else{
        //Minimal speed
        fan_1_rpm = FAN1_RPM_DELTA;
      }
      //Fan need to play by new RPM
      if(fan_1_rpm > 0){
        //Update fan work period
        fan_up_s = FAN_UP_PERIOD_S;
        //Open relay if fan is off
        if (!fan_to_start) {
          fan_to_start = true;
          digitalWrite(RELAY2_PIN, LOW);
          digitalWrite(LED_BLUE_PIN, HIGH);
          #ifdef REM_DEBUG
          Serial.println("FAN START");
          #endif
        }
        //Update fan PWM frequency by duty
        setPwmDuty(OC1A_PIN, fan_1_rpm);
        #ifdef REM_DEBUG
        Serial.print("FAN PWM: ");
        Serial.println(fan_1_rpm);
        #endif
      }
    }

    //Update period avarage values
    if (read_for_period > 0) {
      temp_int_period = ((read_for_period * t_i) + t_i) / (read_for_period + 1);
      hum_int_period = ((read_for_period * h_i) + h_i) / (read_for_period + 1);
      temp_ext_period = ((read_for_period * t_e) + t_e) / (read_for_period + 1);
      hum_ext_period = ((read_for_period * h_e) + h_e) / (read_for_period + 1);
    }
    else {
      temp_int_period = t_i;
      hum_int_period = h_i;
      temp_ext_period = t_e;
      hum_ext_period = h_e;
    }
    ++read_for_period;
  }

  //Manage end of fan work period
  if (fan_to_start && fan_up_s < 1) {
    fan_to_start = false;
    setPwmDuty(OC1A_PIN, 0);
    setPwmDuty(OC1B_PIN, 0);
    digitalWrite(RELAY2_PIN, HIGH);
    digitalWrite(LED_BLUE_PIN, LOW);
    #ifdef REM_DEBUG
    Serial.println("FAN STOP");
    #endif
  }

  //manage end of period
  if (read_for_period >= READ_FOR_PERIOD_CNT) {
    //internal temperature
    period_data[period_data_i].temp_int_i = (int8_t)(temp_int_period);
    period_data[period_data_i].temp_int_f = (uint8_t)(round((temp_int_period - period_data[period_data_i].temp_int_i) * 10));
    if (period_data[period_data_i].temp_int_f > 9) {
      period_data[period_data_i].temp_int_f = 0;
      period_data[period_data_i].temp_int_i += 1;
    }
    //external temperature
    period_data[period_data_i].temp_ext_i = (int8_t)(temp_ext_period);
    period_data[period_data_i].temp_ext_f = (uint8_t)(round((temp_ext_period - period_data[period_data_i].temp_ext_i) * 10));
    if (period_data[period_data_i].temp_ext_f > 9) {
      period_data[period_data_i].temp_ext_f = 0;
      period_data[period_data_i].temp_ext_i += 1;
    }
    //Internal humidity
    period_data[period_data_i].hum_int = (int8_t)(round(hum_int_period));
    //external humidity
    period_data[period_data_i].hum_ext = (int8_t)(round(hum_ext_period));
    
    period_data[period_data_i].pwm_1 = (uint8_t)fan_1_rpm;
    period_data[period_data_i].pwm_2 = (uint8_t)fan_2_rpm;

    if (period_data_cnt < PERIOD_DATA_CNT) {
      ++period_data_cnt;
    }

    #ifdef REM_DEBUG
    Serial.print(period_data_i);
    Serial.print(">> ");
    Serial.print("dT_i: ");
    Serial.print(temp_int_period);
    Serial.print("/");
    Serial.print(period_data[period_data_i].temp_int_i);
    Serial.print(".");
    Serial.print(period_data[period_data_i].temp_int_f);
    Serial.print(" dT_e: ");
    Serial.print(temp_ext_period);
    Serial.print("/");
    Serial.print(period_data[period_data_i].temp_ext_i);
    Serial.print(".");
    Serial.print(period_data[period_data_i].temp_ext_f);
    Serial.print(" | dH_i: ");
    Serial.print(hum_int_period);
    Serial.print("/");
    Serial.print(period_data[period_data_i].hum_int);
    Serial.print(" dH_e: ");
    Serial.print(hum_ext_period);
    Serial.print("/");
    Serial.println(period_data[period_data_i].hum_ext);
    Serial.println("----------------");
    #endif

    //Update index of circular buffer
    period_data_i = (period_data_i + 1) % PERIOD_DATA_CNT;

    //Reset loop values
    read_for_period = 0;
    temp_int_period = 0;
    hum_int_period = 0;
    temp_ext_period = 0;
    hum_ext_period = 0;
  }

  //Manage HTTP connection by ethernet
  if (eth_enable) {
    EthernetClient client = eth_server.available();
    if (client) {
      // an http request ends with a blank line
      boolean currentLineIsBlank = true;
      while (client.connected()) {
        if (client.available()) {
          char c = client.read();
          // if you've gotten to the end of the line (received a newline
          // character) and the line is blank, the http request has ended,
          // so you can send a reply
          if (c == '\n' && currentLineIsBlank) {
            // send a standard http response header
            client.println("HTTP/1.1 200 OK");
            client.println("Content-Type: text/plain");
            client.println("Connection: close");  // the connection will be closed after completion of the response
            //client.println("Refresh: 5");  // refresh the page automatically every 5 sec
            client.println();
            client.println("T;I;P;dT_i;dH_i;dT_e;dH_e;pwm_1;pwm_2;");
            //Current temperature
            client.print("I;");
            client.print(read_for_period);
            client.print(";");
            client.print(read_for_period * READ_INST_S);
            client.print(";");
            client.print(temp_int_period);
            client.print(";");
            client.print(hum_int_period);
            client.print(";");
            client.print(temp_ext_period);
            client.print(";");
            client.print(hum_ext_period);
            client.print(";");
            client.print(fan_1_rpm);
            client.print(";");
            client.print(fan_2_rpm);
            client.println(";");

            //Last periods
            if (period_data_cnt > 0)
            {
              int ii = 0;
              for (int i = 0; i < period_data_cnt; ++i) {
                ii = period_data_i - i - 1;
                if (ii < 0) {
                  ii += PERIOD_DATA_CNT;
                }
                client.print("P");
                client.print(";");
                client.print(ii);
                client.print(";-");
                client.print(((i + 1) * READ_INST_S * READ_FOR_PERIOD_CNT) / 60);
                client.print(";");
                client.print(period_data[ii].temp_int_i);
                client.print(".");
                client.print(period_data[ii].temp_int_f);
                client.print(";");
                client.print(period_data[ii].hum_int);
                client.print(";");
                client.print(period_data[ii].temp_ext_i);
                client.print(".");
                client.print(period_data[ii].temp_ext_f);
                client.print(";");
                client.print(period_data[ii].hum_ext);
                client.print(";");
                client.print(period_data[ii].pwm_1);
                client.print(";");
                client.print(period_data[ii].pwm_2);
                client.println(";");
              }
            }
            break;
          }
          if (c == '\n') {
            // you're starting a new line
            currentLineIsBlank = true;
          } else if (c != '\r') {
            // you've gotten a character on the current line
            currentLineIsBlank = false;
          }
        }
      }

      client.stop();
    }
  }

  delay(1);
}

/**
 * setPwmDuty
 * Set a new PWM duty for timer.
 * 
 * \param pin Tier pin.
 * \param duty  Duty percentile to set at timer.
 */
void setPwmDuty(byte pin, byte duty) {
  if (pin == OC1A_PIN){
    OCR1A = (word) (duty*TCNT1_TOP)/100;
  }
  if (pin == OC1B_PIN){
    OCR1B = (word) (duty*TCNT1_TOP)/100;
  }
}
